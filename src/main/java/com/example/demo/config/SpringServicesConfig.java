/*
 * Algebra labs.
 */
 
package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.persistence.ItemRepository;
import com.example.demo.service.Catalog;
import com.example.demo.service.CatalogImpl;

@Configuration
public class SpringServicesConfig {
	
	// TODO: Inject the repository
	ItemRepository repository;
	
	// TODO: Define the catalog bean

}